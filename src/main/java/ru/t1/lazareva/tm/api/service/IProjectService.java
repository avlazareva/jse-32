package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

}