package ru.t1.lazareva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "change-user-password";

    @NotNull
    private static final String DESCRIPTION = "change password of current user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}